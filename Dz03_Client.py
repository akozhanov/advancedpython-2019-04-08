# coding: utf8

import json
import socket

HOST = 'localhost'
PORT = 8000
BUFFERSIZE = 1024
ENCODING = 'utf-8'


if __name__ == '__main__':
    try:
        sock = socket.socket()
        sock.connect((HOST, PORT))

        print('Client started')

        data = input('Enter data to send: ')

        request = json.dumps(
            {'data': data}
        )

        sock.send(request.encode(ENCODING))
        b_data = sock.recv(BUFFERSIZE)
        response = json.loads(
            b_data.decode(ENCODING)
        )

        print(response)

        sock.close()
    except KeyboardInterrupt:
        print('Client closed')





