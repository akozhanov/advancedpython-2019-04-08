# coding: utf8

import datetime
import json
import re
import csv
from pprint import pprint


def get_value(file):
    # global os_type
    result = []
    os_prod = os_name = os_code = os_type = ''
    with open(file, 'r') as f:
        for line in f.readlines():
            if re.match('Изготовитель системы', line.strip()):
                # result.append([x.strip() for x in line.split(':')])
                # os_prod = result.append(line.split(':')[1].strip())
                os_prod = line.split(':')[1].strip()
            if re.match('Название ОС', line.strip()):
                # result.append( [x.strip() for x in line.split(':')])
                # os_name = result.append(line.split(':')[1].strip())
                os_name = line.split(':')[1].strip()
            if re.match('Код продукта', line.strip()):
                # result.append( [x.strip() for x in line.split(':')])
                # os_code = result.append(line.split(':')[1].strip())
                os_code = line.split(':')[1].strip()
            if re.match('Тип системы', line.strip()):
                # result.append( [x.strip() for x in line.split(':')])
                # os_type = result.append(line.split(':')[1].strip())
                os_type = line.split(':')[1].strip()

        # result.append([os_prod,os_name,os_code,os_type])
        result.append(os_prod)
        result.append(os_name)
        result.append(os_code)
        result.append(os_type)
    return result


def write_to_csv(path, data):
    with open(path, "w", newline='') as csv_file:
        writer = csv.writer(csv_file)
        for line in data:
            writer.writerow(line)


def read_from_csv(path):
    result = []
    with open(path, "r", newline='') as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            result.append(row)
    return result


def write_order_to_json(path, item='', quantity='', price='', buyer='', date=''):
    t_json = {'item': item, 'quantity': quantity, 'price': price, 'buyer': buyer, 'date': date}
    with open(path, 'w') as json_data:  # orders.json
        json.dump(t_json, json_data, indent=4)


def read_order_from_json(path):
    with open(path) as json_data:
        return json.load(json_data)


if __name__ == '__main__':
    pass
    ###Реализовать скрипт для чтения/записи данных в формате csv;
    file_csv = 'main_data.csv'
    main_data = ["Изготовитель системы,Название ОС,Код продукта,Тип системы".split(","), get_value('info_1.txt'),
                 get_value('info_2.txt'), get_value('info_3.txt')]
    write_to_csv(file_csv, main_data)

    pprint(read_from_csv(file_csv))
    ###

    ###Реализовать скрипт для чтения/записи данных в формате json;
    file_json = 'text1.json'
    write_order_to_json(path=file_json, item='item', quantity='quantity', price='price', buyer='buyer',
                        date=str(datetime.datetime.now()))

    pprint(read_order_from_json(file_json))
    ###

    ###Реализовать скрипт для чтения/записи данных в формате yaml;
    # Не успел доделать ДЗ
    ###

    ###Реализовать скрипт для преобразования данных в формате csv в формат json;
    # Не успел доделать ДЗ
    ###

    ###Реализовать скрипт для преобразования данных в формате csv в формат yaml;
    # Не успел доделать ДЗ
    ###


    ###Реализовать скрипт для преобразования данных в формате json в формат yaml.
    # Не успел доделать ДЗ
    ###


