# coding: utf8

import json
import socket

HOST = 'localhost'
PORT = 8000
BUFFERSIZE = 1024
ENCODING = 'utf-8'

INSTALLED_MODULES = [
    'echo',
]

if __name__ == '__main__':
    try:
        sock = socket.socket()
        sock.bind((HOST, PORT))
        sock.listen(3)
        #server_actions = get_server_actions()

        print('Server started')

        while True:
            client, address = sock.accept()
            print(f'Client with address { address } was detected')

            b_request = client.recv(BUFFERSIZE)
            request = json.loads(b_request.decode(ENCODING))

            action_name = request.get('action')

            if validate_request(request):
                controller = resolve(action_name, server_actions)
                if controller:
                    try:
                        response = controller(request)
                    except Exception as err:
                        print(err)
                        response = make_response(
                            request, 500, 'Internal server error'
                        )
                else:
                    print(f'Action with name { action_name } does not exists')
                    response = make_404(request)
            else:
                print(f'Request is not valid')
                response = make_400(request)

            s_response = json.dumps(response)
            client.send(s_response.encode(encoding))

            client.close()
    except KeyboardInterrupt:
        print('Client closed')